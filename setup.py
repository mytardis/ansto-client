from setuptools import setup, find_packages

setup(
    name='anstoapi',
    version='0.0.1',
    packages=find_packages('ansto-api'),
    package_dir={'': 'ansto-api'},
    url='',
    license='',
    author='jason',
    author_email='jason.rigby@monash.edu',
    description='Authn/Authz, file access and proposal db for ANSTO',
    install_requires=[
        'requests',
        'suds',
        'suds_requests',
        'h5py',
        'djangorestframework==3.7.3',
        'markdown',
        'django-filter'
    ],
    tests_require=[
        'xmltodict'
    ]
)
