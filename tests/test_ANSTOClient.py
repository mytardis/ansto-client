from unittest import TestCase

import requests_mock

import mock_ansto_data_api as mock_server
from ansto.api.ansto_data_api import ANSTOClient, ANSTOException


class TestANSTOClient(TestCase):
    def setUp(self):
        self.ansto_client = ANSTOClient(username='mockuser',
                                        password='mockpass',
                                        testing_mode=True)

    def test__full_url(self):
        full_url = self.ansto_client._full_url('test')
        self.assertEqual(full_url, 'https://tardis-auth.nbi.ansto.gov.au/test')

    @requests_mock.mock()
    def test__do_request(self, m):
        m.get('https://tardis-auth.nbi.ansto.gov.au/api/?function=hello',
              text='{"status": "OK", "response": "world"}')
        m.post('https://tardis-auth.nbi.ansto.gov.au/api/',
               text='{"status": "OK", "response": "world!!"}')

        # Test a GET request
        response = self.ansto_client._do_request(parameters={
            'function': 'hello'
        })
        self.assertEqual(response['response'], 'world')

        # Test a POST request
        response = self.ansto_client._do_request(parameters={
            'function': 'hello'
        }, is_post=True)
        self.assertEqual(response['response'], 'world!!')

    @requests_mock.mock()
    def test_authenticate(self, m):
        m.post('https://tardis-auth.nbi.ansto.gov.au/api/',
               json=mock_server.auth_request_callback)
        self.assertTrue(self.ansto_client.authenticate('hardok', 'hardok'))
        self.assertFalse(
            self.ansto_client.authenticate(
                'harderror', 'harderror'))

    @requests_mock.mock()
    def test_list_users_for_proposal(self, m):
        m.get(
            'https://tardis-auth.nbi.ansto.gov.au/api/?function=list_users_for_proposal&proposal=hardok',
            json=mock_server.list_users_for_proposal_callback)
        m.get(
            'https://tardis-auth.nbi.ansto.gov.au/api/?function=list_users_for_proposal&proposal=0000',
            json=mock_server.list_users_for_proposal_callback)

        expected_users = ['user1', 'user2', 'user3', 'user4']
        self.assertEqual(self.ansto_client.list_users_for_proposal('hardok'),
                         expected_users)
        self.assertRaises(ANSTOException,
                          self.ansto_client.list_users_for_proposal, '0000')

    @requests_mock.mock()
    def test_list_proposals_for_user(self, m):
        m.get(
            'https://tardis-auth.nbi.ansto.gov.au/api/?function=list_proposals_for_user&username=hardok',
            json=mock_server.list_proposals_for_user_callback)
        m.get(
            'https://tardis-auth.nbi.ansto.gov.au/api/?function=list_proposals_for_user&username=harderror',
            json=mock_server.list_proposals_for_user_callback)

        expected_proposals = ['1', '2', '1234', '12345', 'hardok']
        self.assertEqual(self.ansto_client.list_proposals_for_user('hardok'),
                         expected_proposals)
        self.assertRaises(
            ANSTOException,
            self.ansto_client.list_proposals_for_user,
            'harderror')

    @requests_mock.mock()
    def test_list_files_for_proposal(self, m):
        m.get(
            'https://tardis-auth.nbi.ansto.gov.au/api/?function=list_files_for_proposal&proposal=hardok',
            json=mock_server.list_files_for_proposal_callback)
        m.get(
            'https://tardis-auth.nbi.ansto.gov.au/api/?function=list_files_for_proposal&proposal=0000',
            json=mock_server.list_files_for_proposal_callback)

        expected_files = [
            "/api/f/experiments/tardis/data/proposal/hardok/QKK0049140.nx.hdf",
            "/api/f/experiments/tardis/data/proposal/hardok/QKK0049140.nx.hdf"]
        self.assertEqual(
            self.ansto_client.list_files_for_proposal('hardok', False),
            expected_files)

        expected_files = [
            "https://tardis-auth.nbi.ansto.gov.au/api/f/experiments/tardis/data/proposal/hardok/QKK0049140.nx.hdf",
            "https://tardis-auth.nbi.ansto.gov.au/api/f/experiments/tardis/data/proposal/hardok/QKK0049140.nx.hdf"]
        self.assertEqual(
            self.ansto_client.list_files_for_proposal('hardok', True),
            expected_files)
        self.assertRaises(ANSTOException,
                          self.ansto_client.list_files_for_proposal, '0000')

    @requests_mock.mock()
    def test_list_files_for_user(self, m):
        m.get(
            'https://tardis-auth.nbi.ansto.gov.au/api/?function=list_files_for_user&username=hardok',
            json=mock_server.list_files_for_user_callback)
        m.get(
            'https://tardis-auth.nbi.ansto.gov.au/api/?function=list_files_for_user&username=harderror',
            json=mock_server.list_files_for_user_callback)

        expected_files = [
            "https://tardis-auth.nbi.ansto.gov.au/api/f/experiments/tardis/data/proposal/hardok/QKK0049140.nx.hdf",
            "https://tardis-auth.nbi.ansto.gov.au/api/f/experiments/tardis/data/proposal/hardok/QKK0049140.nx.hdf"]
        self.assertEqual(self.ansto_client.list_files_for_user('hardok', True),
                         expected_files)

        expected_files = [
            "/api/f/experiments/tardis/data/proposal/hardok/QKK0049140.nx.hdf",
            "/api/f/experiments/tardis/data/proposal/hardok/QKK0049140.nx.hdf"]
        self.assertEqual(
            self.ansto_client.list_files_for_user('hardok', False),
            expected_files)

        self.assertRaises(ANSTOException,
                          self.ansto_client.list_files_for_user, 'harderror')

    @requests_mock.mock()
    def test_list_proposals_to_ingest(self, m):
        m.get(
            'https://tardis-auth.nbi.ansto.gov.au/api/?function=list_proposals_to_ingest&mode=hardok',
            json=mock_server.list_proposals_to_ingest_callback)

        expected_proposals = [
            "hardok"
        ]
        self.assertEqual(self.ansto_client.list_proposals_to_ingest(),
                         expected_proposals)

    @requests_mock.mock()
    def test_flag_proposal_as_ingested(self, m):
        m.get(
            'https://tardis-auth.nbi.ansto.gov.au/api/?function=flag_proposal_as_ingested&proposal=hardok',
            json=mock_server.flag_proposal_as_ingested_callback)
        m.get(
            'https://tardis-auth.nbi.ansto.gov.au/api/?function=flag_proposal_as_ingested&proposal=0000',
            json=mock_server.flag_proposal_as_ingested_callback)

        expected_success_response = ["hardok"]
        self.assertEqual(self.ansto_client.flag_proposal_as_ingested("hardok"),
                         expected_success_response)

        self.assertRaises(ANSTOException,
                          self.ansto_client.flag_proposal_as_ingested, '0000')

    @requests_mock.mock()
    def test_flag_proposal_for_ingestion(self, m):
        m.get(
            'https://tardis-auth.nbi.ansto.gov.au/api/?function=flag_proposal_for_ingestion&proposal=hardok',
            json=mock_server.flag_proposal_for_ingestion_callback)
        m.get(
            'https://tardis-auth.nbi.ansto.gov.au/api/?function=flag_proposal_for_ingestion&proposal=0000',
            json=mock_server.flag_proposal_for_ingestion_callback)

        expected_success_response = ["hardok"]
        self.assertEqual(self.ansto_client.flag_proposal_for_ingestion("hardok"),
                         expected_success_response)

        self.assertRaises(ANSTOException,
                          self.ansto_client.flag_proposal_for_ingestion, '0000')