import xmltodict


def _soap_body(request_text):
    xml_req = xmltodict.parse(request_text)
    return xml_req['SOAP-ENV:Envelope']['ns0:Body']


def get_booking_details_callback(request, context):
    req_body = _soap_body(request.text)
    if req_body['ns1:getBookingDetailsElement']['ns1:proposalId'] == '3015' \
            and req_body['ns1:getBookingDetailsElement'][
                'ns1:instrumentId'] == "6":
        with open('soap_xml/test_get_booking_details.xml', 'r') as f:
            return f.read()
    else:
        context.status_code = 400
        return "Bad request"


def get_bragg_info_callback(request, context):
    req_body = _soap_body(request.text)
    if req_body['ns1:getBraggInfoElement'] is None:
        with open('soap_xml/test_get_bragg_info.xml', 'r') as f:
            return f.read()
    else:
        context.status_code = 400
        return "Bad request"


def get_co_investigators_callback(request, context):
    req_body = _soap_body(request.text)
    if req_body['ns1:getCoInvestigatorsElement']['ns1:proposalId'] == '3015':
        with open('soap_xml/test_get_co_investigators.xml', 'r') as f:
            return f.read()
    else:
        context.status_code = 400
        return "Bad request"


def get_principal_investigator_id_callback(request, context):
    req_body = _soap_body(request.text)
    if req_body['ns1:getPrincipalInvestigatorIDElement'][
            'ns1:proposalId'] == '3015':
        with open('soap_xml/test_get_principal_investigator_id.xml', 'r') as f:
            return f.read()
    else:
        context.status_code = 400
        return "Bad request"


def get_proposal_id_callback(request, context):
    req_body = _soap_body(request.text)
    if req_body['ns1:getProposalIdElement']['ns1:proposalCode'] == 'P3015':
        with open('soap_xml/test_get_proposal_id.xml', 'r') as f:
            return f.read()
    else:
        context.status_code = 400
        return "Bad request"


def get_proposal_info_callback(request, context):
    req_body = _soap_body(request.text)
    if req_body['ns1:getProposalInfoElement']['ns1:proposalId'] == '3015':
        with open('soap_xml/test_get_proposal_info.xml', 'r') as f:
            return f.read()
    else:
        context.status_code = 400
        return "Bad request"


def get_proposal_references_callback(request, context):
    req_body = _soap_body(request.text)
    if req_body['ns1:getProposalReferencesElement'][
            'ns1:proposalId'] == '3015':
        with open('soap_xml/test_get_proposal_references.xml', 'r') as f:
            return f.read()
    else:
        context.status_code = 400
        return "Bad request"


def get_proposal_round_callback(request, context):
    req_body = _soap_body(request.text)
    if req_body['ns1:getProposalRoundElement']['ns1:proposalId'] == '3015':
        with open('soap_xml/test_get_proposal_round.xml', 'r') as f:
            return f.read()
    else:
        context.status_code = 400
        return "Bad request"


def get_proposal_tags_callback(request, context):
    req_body = _soap_body(request.text)
    if req_body['ns1:getProposalTagsElement']['ns1:proposalId'] == '3015':
        with open('soap_xml/test_get_proposal_tags.xml', 'r') as f:
            return f.read()
    else:
        context.status_code = 400
        return "Bad request"


def get_proposal_text_callback(request, context):
    req_body = _soap_body(request.text)
    if req_body['ns1:getProposalTextElement']['ns1:proposalId'] == '3015':
        with open('soap_xml/test_get_proposal_text.xml', 'r') as f:
            return f.read()
    else:
        context.status_code = 400
        return "Bad request"


def get_proposal_title_callback(request, context):
    req_body = _soap_body(request.text)
    if req_body['ns1:getProposalTitleElement']['ns1:proposalId'] == '3015':
        with open('soap_xml/test_get_proposal_title.xml', 'r') as f:
            return f.read()
    else:
        context.status_code = 400
        return "Bad request"


def get_reactor_display_callback(request, context):
    req_body = _soap_body(request.text)
    if req_body['ns1:getReactorDisplayElement'] is None:
        with open('soap_xml/test_get_reactor_display.xml', 'r') as f:
            return f.read()
    else:
        context.status_code = 400
        return "Bad request"


def get_reactor_power_callback(request, context):
    req_body = _soap_body(request.text)
    if req_body['ns1:getReactorPowerElement'] is None:
        with open('soap_xml/test_get_reactor_power.xml', 'r') as f:
            return f.read()
    else:
        context.status_code = 400
        return "Bad request"


def get_reactor_simple_display_callback(request, context):
    req_body = _soap_body(request.text)
    if req_body['ns1:getReactorSimpleDisplayElement'] is None:
        with open('soap_xml/test_get_reactor_simple_display.xml', 'r') as f:
            return f.read()
    else:
        context.status_code = 400
        return "Bad request"


def get_scientific_area_callback(request, context):
    req_body = _soap_body(request.text)
    if req_body['ns1:getScientificAreaElement']['ns1:proposalId'] == '3015':
        with open('soap_xml/test_get_scientific_area.xml', 'r') as f:
            return f.read()
    else:
        context.status_code = 400
        return "Bad request"


def validate_proposal_callback(request, context):
    req_body = _soap_body(request.text)
    if req_body['ns1:validateProposalElement']['ns1:proposalId'] == '3015' \
            and req_body['ns1:validateProposalElement'][
                'ns1:email'] == 'gaili@ansto.gov.au':
        with open('soap_xml/test_validate_proposal.xml', 'r') as f:
            return f.read()
    else:
        context.status_code = 400
        return "Bad request"
