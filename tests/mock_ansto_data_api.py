INVALID_TARDIS_API_AUTH = "Invalid Tardis API auth"


def require_auth(func):
    """
    A decorator for tests that require auth
    """
    def func_wrapper(request, context):
        if not server_auth_ok(request, context):
            return lambda _request, _context: INVALID_TARDIS_API_AUTH
        return func(request, context)
    return func_wrapper


def server_auth_ok(request, context):
    if request.headers['Authorization'] != 'Basic bW9ja3VzZXI6bW9ja3Bhc3M=':
        context.status_code = 401
        return False
    else:
        return True


@require_auth
def auth_request_callback(request, context):
    if request.text == 'function=authenticate&username=hardok&password=hardok':
        return {
            "status": "OK",
            "timestamp": "2015-11-12T17:07:25"
        }
    else:
        context.status_code = 401
        return {
            "status": "ERROR: Authentication failure",
            "timestamp": "2015-11-12T17:07:47"
        }


@require_auth
def list_users_for_proposal_callback(request, context):
    expected_query = {
        'function': ['list_users_for_proposal'],
        'proposal': ['hardok']
    }

    if request.qs == expected_query:
        return {
            "status": "OK",
            "timestamp": "2015-11-12T17:11:11",
            "list_users_for_proposal": [
                "user1",
                "user2",
                "user3",
                "user4"]
        }
    else:
        context.status_code = 404
        return {
            "status": "ERROR: Can't find users for proposal '0000'",
            "timestamp": "2015-11-12T17:12:02"
        }


@require_auth
def list_proposals_for_user_callback(request, context):
    expected_query = {
        'function': ['list_proposals_for_user'],
        'username': ['hardok']
    }

    if request.qs == expected_query:
        return {
            "status": "OK",
            "timestamp": "2015-11-12T17:12:36",
            "list_proposals_for_user": [
                "1",
                "2",
                "1234",
                "12345"]
        }
    else:
        context.status_code = 404
        return {
            "status": "ERROR: Username 'harderror' not found",
            "timestamp": "2015-11-12T17:13:02"
        }


@require_auth
def list_files_for_proposal_callback(request, context):
    expected_query = {
        'function': ['list_files_for_proposal'],
        'proposal': ['hardok']
    }

    if request.qs == expected_query:
        return {
            "status": "OK",
            "timestamp": "2015-11-12T17:13:21",
            "list_files_for_proposal": [
                "/api/f/experiments/tardis/data/proposal/hardok/QKK0049140.nx.hdf",
                "/api/f/experiments/tardis/data/proposal/hardok/QKK0049140.nx.hdf"]}
    else:
        context.status_code = 404
        return {
            "status": "ERROR: No files found for proposal '0000'",
            "timestamp": "2015-11-12T17:13:47"
        }


@require_auth
def list_files_for_user_callback(request, context):
    expected_query = {
        'function': ['list_files_for_user'],
        'username': ['hardok']
    }

    if request.qs == expected_query:
        return {
            "status": "OK",
            "timestamp": "2015-11-12T17:13:21",
            "list_files_for_user": [
                "/api/f/experiments/tardis/data/proposal/hardok/QKK0049140.nx.hdf",
                "/api/f/experiments/tardis/data/proposal/hardok/QKK0049140.nx.hdf"]}
    else:
        context.status_code = 404
        return {
            "status": "ERROR: No files found for username 'harderror'",
            "timestamp": "2015-11-12T17:13:47"
        }


@require_auth
def list_proposals_to_ingest_callback(request, context):
    expected_query = {
        'mode': ['hardok'],
        'function': ['list_proposals_to_ingest']
    }

    if request.qs == expected_query:
        return {
            "status": "OK",
            "timestamp": "2016-03-24T14:40:24",
            "list_proposals_to_ingest": ["hardok"]}
    else:
        context.status_code = 404
        return {
            "status": "ERROR: proposal '0000' not found",
            "timestamp": "2016-03-30T17:01:36"
        }


@require_auth
def flag_proposal_as_ingested_callback(request, context):
    expected_query = {
        'function': ['flag_proposal_as_ingested'],
        'proposal': ['hardok']
    }

    if request.qs == expected_query:
        return {
            "status": "OK",
            "timestamp": "2016-03-30T16:27:39",
            "flag_proposal_as_ingested": ["hardok"]}
    else:
        context.status_code = 404
        return {
            "status": "ERROR: proposal '0000' not found",
            "timestamp": "2016-03-30T17:01:36"}

@require_auth
def flag_proposal_for_ingestion_callback(request, context):
    expected_query = {
        'function': ['flag_proposal_for_ingestion'],
        'proposal': ['hardok']
    }

    if request.qs == expected_query:
        return {
            "status": "OK",
            "timestamp": "2016-09-22T10:47:07",
            "flag_proposal_for_ingestion": ["hardok"]}
    else:
        context.status_code = 404
        return {
            "status": "ERROR: proposal '0000' not found",
            "timestamp": "2016-09-22T10:53:42"}
