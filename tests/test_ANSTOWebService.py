import datetime
from dateutil.tz import tzlocal
from unittest import TestCase

import requests_mock

import mock_ansto_webservice as mock_ws
from ansto.api.ansto_webservice import ANSTOWebService

WS_WSDL = 'http://neutron.ansto.gov.au/WebServices/WebServiceAppServiceSoapHttpPort?WSDL'

WS_ENDPOINT = 'http://neutron.ansto.gov.au/WebServices/WebServiceAppServiceSoapHttpPort'


class TestANSTOWebService(TestCase):
    @requests_mock.mock()
    def setUp(self, m):
        with open('soap_xml/test_wsdl.xml', 'r') as f:
            m.get(WS_WSDL, text=f.read())
        self.ws = ANSTOWebService()

    @requests_mock.mock()
    def test_get_booking_details(self, m):
        m.post(WS_ENDPOINT, text=mock_ws.get_booking_details_callback)
        booking_details = self.ws.get_booking_details("3015", "6")
        self.assertEqual(booking_details[0]['groupId'], 5375)
        self.assertEqual(booking_details[1]['groupId'], 6258)

    @requests_mock.mock()
    def test_get_bragg_info(self, m):
        m.post(WS_ENDPOINT, text=mock_ws.get_bragg_info_callback)
        bragg_info = self.ws.get_bragg_info()
        self.assertEqual(
            bragg_info['screenMessage'],
            'OPAL shutdown scheduled for Monday 7th December at 2:00am')

    @requests_mock.mock()
    def test_get_co_investigators(self, m):
        m.post(WS_ENDPOINT, text=mock_ws.get_co_investigators_callback)
        co_investigators = self.ws.get_co_investigators("3015")
        self.assertEqual(co_investigators, [3201, 3202, 3203])

    @requests_mock.mock()
    def test_get_principal_investigator_id(self, m):
        m.post(WS_ENDPOINT,
               text=mock_ws.get_principal_investigator_id_callback)
        principal_investigator_id = self.ws \
            .get_principal_investigator_id("3015")
        self.assertEqual(principal_investigator_id, 3154)

    @requests_mock.mock()
    def test_get_proposal_id(self, m):
        m.post(WS_ENDPOINT, text=mock_ws.get_proposal_id_callback)
        proposal_id = self.ws.get_proposal_id("P3015")
        self.assertEqual(proposal_id, "3015")

    @requests_mock.mock()
    def test_get_proposal_info(self, m):
        m.post(WS_ENDPOINT, text=mock_ws.get_proposal_info_callback)
        proposal_info = self.ws.get_proposal_info("3015")
        self.assertEqual(proposal_info['proposalCode'], 'P3015')

    @requests_mock.mock()
    def test_get_proposal_references(self, m):
        m.post(WS_ENDPOINT, text=mock_ws.get_proposal_references_callback)
        proposal_references = self.ws.get_proposal_references("3015")
        self.assertEqual(proposal_references, 'References placeholder text')

    @requests_mock.mock()
    def test_get_proposal_round(self, m):
        m.post(WS_ENDPOINT, text=mock_ws.get_proposal_round_callback)
        proposal_round = self.ws.get_proposal_round("3015")
        self.assertEqual(proposal_round, '2013-2 Neutron')

    @requests_mock.mock()
    def test_get_proposal_tags(self, m):
        m.post(WS_ENDPOINT, text=mock_ws.get_proposal_tags_callback)
        proposal_tags = self.ws.get_proposal_tags("3015")
        self.assertEqual(proposal_tags, ['kesterites',
                                         'koala',
                                         'single-crystal laue diffraction',
                                         'solar cells'])

    @requests_mock.mock()
    def test_get_proposal_text(self, m):
        # todo: pick a better test with actual content
        m.post(WS_ENDPOINT, text=mock_ws.get_proposal_text_callback)
        proposal_text = self.ws.get_proposal_text("3015")
        self.assertIsNone(proposal_text)

    @requests_mock.mock()
    def test_get_proposal_title(self, m):
        m.post(WS_ENDPOINT, text=mock_ws.get_proposal_title_callback)
        proposal_title = self.ws.get_proposal_title("3015")
        self.assertEqual(proposal_title,
                         'Extension of the phase diagram of kesterite \
materials for solar cell technology')

    @requests_mock.mock()
    def test_get_reactor_display(self, m):
        m.post(WS_ENDPOINT, text=mock_ws.get_reactor_display_callback)
        reactor_display = self.ws.get_reactor_display()
        self.assertEqual(reactor_display['timeStamp'],
                         datetime.datetime(2015, 11, 27, 23, 34, 41))

    @requests_mock.mock()
    def test_get_reactor_power(self, m):
        m.post(WS_ENDPOINT, text=mock_ws.get_reactor_power_callback)
        reactor_power = self.ws.get_reactor_power()
        self.assertEqual(reactor_power['value'], "19.433533")

    @requests_mock.mock()
    def test_get_reactor_simple_display(self, m):
        m.post(WS_ENDPOINT, text=mock_ws.get_reactor_simple_display_callback)
        reactor_simple_display = self.ws.get_reactor_simple_display()
        self.assertEqual(reactor_simple_display,
                         '2015-11-28 12:36:42; Power: 19.438145; CNS in: \
20.2339; CNS out: 27.41; TG123: Open; CG123: Open; TG4: Open; CG4: Open')

    @requests_mock.mock()
    def test_get_scientific_area(self, m):
        m.post(WS_ENDPOINT, text=mock_ws.get_scientific_area_callback)
        scientific_area = self.ws.get_scientific_area("3015")
        self.assertEqual(scientific_area, 'Crystallography')

    @requests_mock.mock()
    def test_validate_proposal(self, m):
        m.post(WS_ENDPOINT, text=mock_ws.validate_proposal_callback)
        is_valid_proposal = self.ws.validate_proposal("3015",
                                                      "gaili@ansto.gov.au")
        self.assertTrue(is_valid_proposal)

    @requests_mock.mock()
    def test_get_proposal_date_range(self, m):
        m.post(WS_ENDPOINT, text=mock_ws.get_proposal_info_callback)

        dates = self.ws.get_proposal_date_range("3015")
        self.assertEqual(dates,
                         (datetime.datetime(2013, 10, 8, tzinfo=tzlocal()),
                          datetime.datetime(2013, 10, 28, tzinfo=tzlocal()))
                         )

    @requests_mock.mock()
    def test_get_proposal_bookings(self, m):
        m.post(WS_ENDPOINT, text=mock_ws.get_proposal_info_callback)

        dates = self.ws.get_proposal_bookings("3015")
        self.assertEqual(dates, [
            {
                'start': datetime.datetime(2013, 10, 8, tzinfo=tzlocal()),
                'end': datetime.datetime(2013, 10, 12, tzinfo=tzlocal()),
                'instrument': 'KOALA'
            },
            {
                'start': datetime.datetime(2013, 10, 23, tzinfo=tzlocal()),
                'end': datetime.datetime(2013, 10, 28, tzinfo=tzlocal()),
                'instrument': 'KOALA'
            }
        ])

    @requests_mock.mock()
    def test_get_proposal_investigators(self, m):
        m.post(WS_ENDPOINT, text=mock_ws.get_proposal_info_callback)

        investigators = self.ws.get_proposal_investigators("3015")
        self.assertEqual(investigators, [
            ('Gail N. Iles', 'ANSTO - BI', 'gaili@ansto.gov.au'),
            ('Galina Gurieva', 'Helmholtz Zentrum Berlin', 'galina.gurieva@helmholtz-berlin.de'),
            ('Sergiu Levcenco', 'Helmholtz Zentrum Berlin', 'sergiu.levcenco@helmholtz-berlin.de'),
            ('Susan Schorr', 'Helmholtz Zentrum Berlin', 'susan.schorr@helmholtz-berlin.de')
        ])
