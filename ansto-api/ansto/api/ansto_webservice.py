import re

from datetime import datetime
from dateutil.tz import tzlocal
import suds_requests
from suds.client import Client, WebFault

WSDL = 'http://neutron.ansto.gov.au/WebServices/WebServiceAppServiceSoapHttpPort?WSDL'


class ANSTOWebService:
    def __init__(self, wsdl=WSDL):
        self.client = Client(wsdl, transport=suds_requests.RequestsTransport())

    def get_booking_details(self, proposal_id, instrument_id):
        return self.client.service \
            .getBookingDetails(proposalId=proposal_id,
                               instrumentId=instrument_id)

    def get_bragg_info(self):
        return self.client.service.getBraggInfo()

    def get_co_investigators(self, proposal_id):
        return self.client.service.getCoInvestigators(proposalId=proposal_id)

    def get_principal_investigator_id(self, proposal_id):
        return self.client.service \
            .getPrincipalInvestigatorID(proposalId=proposal_id)

    def get_proposal_id(self, proposal_code):
        return self.client.service.getProposalId(proposalCode=proposal_code)

    def get_proposal_info(self, proposal_id):
        return self.client.service.getProposalInfo(proposalId=proposal_id)

    def get_proposal_references(self, proposal_id):
        return self.client.service \
            .getProposalReferences(proposalId=proposal_id)

    def get_proposal_round(self, proposal_id):
        return self.client.service.getProposalRound(proposalId=proposal_id)

    def get_proposal_tags(self, proposal_id):
        return self.client.service.getProposalTags(proposalId=proposal_id)

    def get_proposal_text(self, proposal_id):
        return self.client.service.getProposalText(proposalId=proposal_id)

    def get_proposal_title(self, proposal_id):
        return self.client.service.getProposalTitle(proposalId=proposal_id)

    def get_reactor_display(self):
        return self.client.service.getReactorDisplay()

    def get_reactor_power(self):
        return self.client.service.getReactorPower()

    def get_reactor_simple_display(self):
        return self.client.service.getReactorSimpleDisplay()

    def get_scientific_area(self, proposal_id):
        return self.client.service.getScientificArea(proposalId=proposal_id)

    def validate_proposal(self, proposal_id, email):
        return self.client.service \
            .validateProposal(proposalId=proposal_id, email=email)

    def get_proposal_bookings(self, proposal_id):
        def add_tz(dt):
            return dt.replace(tzinfo=tzlocal())
        date_pattern = "%d/%m/%Y"
        proposal_info = self.get_proposal_info(proposal_id)
        bookings = []
        try:
            for booking in proposal_info['bookingArray']:
                start = add_tz(datetime.strptime(booking['startDate'],
                                                 date_pattern))
                end = add_tz(
                    datetime.strptime(booking['endDate'], date_pattern))
                bookings.append({
                    'start': start,
                    'end': end,
                    'instrument': booking['instrument']
                })
        except AttributeError:
            pass
        return bookings

    def get_proposal_date_range(self, proposal_id):
        '''
        Get the start and end dates for a proposal id
        :param proposal_id: the proposal it
        :return: a tuple with the start and end dates
        '''
        earliest_start = None
        latest_end = None
        for booking in self.get_proposal_bookings(proposal_id):
            start = booking['start']
            end = booking['end']
            if earliest_start is None or start < earliest_start:
                earliest_start = start
            if latest_end is None or end > latest_end:
                latest_end = end
        return earliest_start, latest_end

    def get_proposal_investigators(self, proposal_id):
        '''
        Returns a list of tuples containing the name, institution and email
        address of the proposal investigators. Values are derived from the
        getProposalInfo function of the proposals database. The PI is always
        the first investigator in the list. If there are no authors, None is
        returned.
        :param proposal_id: the proposal id
        :return: a list of investigators or None
        '''
        def split_name_institution(name):
            p = re.compile(r'^(?P<name>.+) \((?P<institution>.+)\)$')
            m = p.search(name)
            if m is None:
                return name, '' # Handle unexpected pattern matching failure
            else:
                return m.group('name'), m.group('institution')
        try:
            proposal_info = self.get_proposal_info(proposal_id)
        except WebFault:
            return None

        if not proposal_info:
            return None
        pi_name = proposal_info['principalSci']
        pi_email = proposal_info['principalEmail']
        try:
            co_investigator_names = proposal_info['otherSci'].split(';')
            co_investigator_emails = proposal_info['otherEmail'].split(';')
        except AttributeError:
            co_investigator_names = []
            co_investigator_emails = []

        assert len(co_investigator_names) == len(co_investigator_emails)
        investivators = []
        for _name, email in zip([pi_name] + co_investigator_names,
                               [pi_email] + co_investigator_emails):
            name, institution = split_name_institution(_name)
            investivators.append((name, institution, email))
        return investivators
