import json
import logging

import requests
from requests.auth import HTTPBasicAuth
from requests.exceptions import HTTPError

logger = logging.getLogger(__name__)


class ANSTOException(Exception):
    def __init__(self, message, status_code=None):
        if status_code:
            msg = "(%i) %s" % (status_code, message)
        else:
            msg = message

        super(Exception, self).__init__(msg)


class ANSTOClient:
    """
    Provides an interface to the ANSTO API v2
    """

    def __init__(self, username, password,
                 base_url="https://tardis-auth.nbi.ansto.gov.au/",
                 testing_mode=False):
        """
        ANSTOClient constructor
        :param username: API user name
        :param password: API password
        :param base_url: Base URL of the API server
        :return:
        """
        self.auth = HTTPBasicAuth(username, password)
        self.username = username
        self.password = password
        self.base_url = base_url
        self.testing_mode = testing_mode

    @staticmethod
    def get_initialised_client():
        from django.core.exceptions import ImproperlyConfigured
        try:
            kwargs = {}
            from django.conf import settings
            if hasattr(settings, 'ANSTO_API_BASE_URL'):
                kwargs['base_url'] = getattr(settings, 'ANSTO_API_BASE_URL')
            kwargs['testing_mode'] = getattr(settings, 'ANSTO_TESTING_MODE',
                                             False)
            return ANSTOClient(
                settings.ANSTO_API_USERNAME,
                settings.ANSTO_API_PASSWORD,
                **kwargs)
        except AttributeError:
            raise ImproperlyConfigured(
                'ANSTO API client credentials not configured. '
                'Set ANSTO_API_USERNAME and ANSTO_API_PASSWORD.')

    def _full_url(self, uri):
        """
        Joins the base URL to the URI
        :param uri: URI of the resource
        :return: complete URL
        """
        return ''.join([self.base_url.strip('/'), '/', uri.lstrip('/')])

    def _do_request(self, parameters, is_post=False):
        """
        Initiates a request with the ANSTO API
        :param parameters: parameters to be included in the request
        :param is_post: True if the request is by POST, or False if GET
        :return: an object containing the JSON values returned
        """

        url = self._full_url('api/')
        if is_post:
            result = requests.post(url, data=parameters,
                                   auth=self.auth)
        else:
            result = requests.get(url, params=parameters,
                                  auth=self.auth)

        logger.debug("ANSTO API client sent: %s" % json.dumps(parameters))
        logger.debug("ANSTO API server said: %s" % result.text)

        result_json = result.json()

        # Case where status codes are used
        try:
            result.raise_for_status()
        except HTTPError:
            raise ANSTOException(result_json['status'], result.status_code)

        # Case where 'status' response is not OK
        if result_json['status'] != 'OK':
            raise ANSTOException(result_json['status'])

        return result_json

    def authenticate(self, username, password):
        """
        Authenticates an ANSTO user with the API
        :param username: the user's user name
        :param password: the user's password
        :return: True if authentication successful, False otherwise
        """
        params = {
            'function': 'authenticate',
            'username': username,
            'password': password
        }
        try:
            self._do_request(params, is_post=True)
        except ANSTOException as e:
            logger.error("Authentication for user '%s' failed. %s" %
                         (username, e.message))
            return False
        return True

    def list_users_for_proposal(self, proposal_id):
        """
        Lists the users belonging to a given proposal
        :param proposal_id: the proposal name
        :return: a list of users
        """
        params = {
            'function': 'list_users_for_proposal',
            'proposal': proposal_id
        }
        return self._do_request(params)['list_users_for_proposal']

    def list_proposals_for_user(self, username):
        """
        Lists the proposals a user belongs to
        :param username: the user's user name
        :return: a list of proposals
        """
        params = {
            'function': 'list_proposals_for_user',
            'username': username
        }
        result = self._do_request(params)['list_proposals_for_user']

        # Add an extra proposal if the client is in testing mode
        if self.testing_mode and username == 'hardok':
            result = [x for x in result] + ['hardok']

        return result

    def list_files_for_proposal(self, proposal_id, full_url=False):
        """
        Lists all the files for a given proposal
        :param proposal_id: the proposal name
        :return: a list of URLs pointing to the files for the proposal
        """
        params = {
            'function': 'list_files_for_proposal',
            'proposal': proposal_id
        }

        return [self._full_url(uri) if full_url else uri
                for uri in self._do_request(params)['list_files_for_proposal']]

    def list_files_for_user(self, username, full_url=False):
        """
        Lists files for a given user
        :param username: the user's user name
        :param full_url: return the full url or just uri
        :return: a list of URLs pointing to the files for the user
        """
        params = {
            'function': 'list_files_for_user',
            'username': username
        }
        return [self._full_url(uri) if full_url else uri
                for uri in self._do_request(params)['list_files_for_user']]

    def list_proposals_to_ingest(self):
        """
        Lists the proposals awaiting ingestion
        :return: a list of proposal ids
        """
        params = {
            'function': 'list_proposals_to_ingest'
        }
        if self.testing_mode:
            params['mode'] = 'hardok'
        else:
            params['mode'] = 'live'

        return self._do_request(params)['list_proposals_to_ingest']

    def flag_proposal_as_ingested(self, proposal_id):
        """
        Notifies that a proposal has been successfully ingested
        :param proposal_id: the proposal id
        :return: a status string
        """
        params = {
            'function': 'flag_proposal_as_ingested',
            'proposal': proposal_id
        }
        return self._do_request(params)['flag_proposal_as_ingested']

    def flag_proposal_for_ingestion(self, proposal_id):
        """
        Registers a proposal id for reingestion
        :param proposal_id: the proposal id
        :return: a list of proposals flagged for ingestion
        """
        params = {
            'function': 'flag_proposal_for_ingestion',
            'proposal': proposal_id
        }
        return self._do_request(params)['flag_proposal_for_ingestion']
