import logging
from string import lower

from django.conf import settings
from django.contrib.auth.models import User

from ansto.api.ansto_data_api import ANSTOException, ANSTOClient
from ansto.tardis import default_settings
from tardis.tardis_portal.auth.interfaces import GroupProvider, AuthProvider, \
    UserProvider
from tardis.tardis_portal.models import UserAuthentication, \
    ExperimentParameterSet, ObjectACL

logger = logging.getLogger('tardis.ansto_auth')

PROPOSAL_LIST = '_proposal_list'

auth_key = u'anoto-auth'
auth_display_name = u'ANSTO'


class ANSTOMiddleware(object):
    def process_request(self, request):
        if PROPOSAL_LIST in request.session:
            request.user.proposal_list = request.session[PROPOSAL_LIST]


class ANSTOGroupProvider(GroupProvider):
    name = u'ansto_group'

    def __init__(self):
        self.ansto_data_api = ANSTOClient.get_initialised_client()

    def _proposal_to_string(self, proposal):
        return "Proposal_%s" % str(proposal)

    def getGroups(self, user):
        """
        Return an the available proposals for a user
        """

        # the user needs to be authenticated
        if not user.is_authenticated():
            return []

        # check if the user is linked to any experiments
        if not hasattr(user, 'proposal_list'):

            # check if a user profile
            try:
                userAuth = UserAuthentication.objects.get(
                    userProfile__user=user,
                    authenticationMethod=auth_key)
            except UserAuthentication.DoesNotExist:
                return []

            # return the list of proposals
            return self.ansto_data_api.list_proposals_for_user(
                userAuth.username)

        # the proposals should be stored in the session if the user
        # is already authenticated
        return user.proposal_list

    def getGroupById(self, id):
        """
        return the group associated with the id::

            {"id": 123,
            "display": "Group Name",}

        """
        return {'id': id,
                'display': self._proposal_to_string(id)}

    def searchGroups(self, **filter):
        proposal = filter.get('name')
        if not proposal:
            return []

        users = self.ansto_data_api.list_users_for_proposal(proposal)
        if len(users) > 0:
            return [
                {
                    'id': int(proposal),
                    'display': self._proposal_to_string(proposal),
                    'members': users
                }
            ]
        else:
            return []

    def getGroupsForEntity(self, entity):
        """
        return a list of the proposal for a particular user

           [{'name': 'Group 456', 'id': '2'},
           {'name': 'Group 123', 'id': '1'}]

        """
        result = self.ansto_data_api.list_proposals_for_user(entity)
        if len(result) > 0:
            return [{'id': proposal, 'name': self._proposal_to_string(
                proposal)} for proposal in result]
        else:
            return []


class Backend(object, AuthProvider, UserProvider):
    """
    Authenticate against the ANSTO API

    a new local user is created if it doesn't already exist

    """

    def __init__(self):
        self.ansto_data_api = ANSTOClient.get_initialised_client()

    def getUserById(self, id):
        pass

    def searchUsers(self, **filter):
        pass

    def authenticate(self, request):
        username = lower(request.POST['username'])
        password = request.POST['password']

        if not username or not password:
            return None

        # authenticate user and update group memberships
        if not self.ansto_data_api.authenticate(username, password):
            return None

        # result contains comma separated list of epns the user is
        # allowed to see
        try:
            proposals = self.ansto_data_api.list_proposals_for_user(username)
        except ANSTOException as e:
            proposals = []
            logger.error("Could not retrieve proposals for user %s: %s" % (
                username, e.message))
        request.session[PROPOSAL_LIST] = proposals
        request.user.proposal_list = proposals

        # need to make sure ObjectACLs exist for all proposals
        for proposal in proposals:
            try:
                # create vbl group
                exp = ExperimentParameterSet.objects.get(
                    schema__namespace=getattr(settings,
                                              'ANSTO_SCHEMA_NAMESPACE',
                                              default_settings.ANSTO_EXPERIMENT_SCHEMA_NAMESPACE),
                    experimentparameter__string_value=proposal,
                    experimentparameter__name__name='Proposal').experiment
                acls = ObjectACL.objects.filter(
                    content_type=exp.get_ct(),
                    object_id=exp.id,
                    pluginId='ansto_group',
                    entityId=proposal,
                    canRead=True,
                    aclOwnershipType=ObjectACL.SYSTEM_OWNED)
                if acls.count() == 0:
                    acl = ObjectACL(content_type=exp.get_ct(),
                                    object_id=exp.id,
                                    pluginId='ansto_group',
                                    entityId=proposal,
                                    canRead=True,
                                    aclOwnershipType=ObjectACL.SYSTEM_OWNED)
                    acl.save()

                from django.contrib.auth.models import Group
                from tardis.tardis_portal.auth.localdb_auth import django_group

                beamline_group = "ANSTO_GROUP"
                group, created = Group.objects.get_or_create(
                    name=beamline_group)

                acl = ObjectACL(content_type=exp.get_ct(),
                                object_id=exp.id,
                                pluginId=django_group,
                                entityId=str(group.id),
                                canRead=True,
                                aclOwnershipType=ObjectACL.SYSTEM_OWNED)
                acl.save()

                group, created = Group.objects.get_or_create(name='admin')
                acl = ObjectACL(content_type=exp.get_ct(),
                                object_id=exp.id,
                                pluginId=django_group,
                                entityId=str(group.id),
                                isOwner=True,
                                canRead=True,
                                aclOwnershipType=ObjectACL.SYSTEM_OWNED)
                acl.save()

            except ExperimentParameterSet.DoesNotExist:
                pass

        return self._make_user_obj(username)

    def get_user(self, user_id):
        pass

    def getUserInfo(self, user_id):
        return self.get_user(user_id)

    # UserProvider
    def getUsernameByEmail(self, email):
        return email

    def _make_user_obj(self, username):
        user,created = User.objects.get_or_create(
            username=username
        )
        user.save()

        if created:
            user.user_permissions.all().delete()

        return user
