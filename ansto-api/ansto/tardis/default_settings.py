ANSTO_FILE_REGEX = ur'\/api\/f\/(.+)'

ANSTO_EXPERIMENT_SCHEMA_NAMESPACE= "https://tardis.nbi.ansto.gov.au/schema/experiment/"
ANSTO_BOOKING_SCHEMA_NAMESPACE= "https://tardis.nbi.ansto.gov.au/schema/experiment/booking/"
ANSTO_DATASET_SCHEMA_NAMESPACE= "https://tardis.nbi.ansto.gov.au/schema/dataset/"

ANSTO_API_ADMIN_USER = "ansto"