import logging
import os
import re
import tarfile
from contextlib import contextmanager

import h5py
import magic
from celery.task import task
from django.conf import settings
from django.contrib.auth.models import User, Group
from django.core.cache import caches
from django.core.exceptions import ImproperlyConfigured
from django.core.files.storage import FileSystemStorage
from django.db import transaction

from ansto.api.ansto_data_api import ANSTOClient
from ansto.api.ansto_webservice import ANSTOWebService
from ansto.tardis import default_settings
from ansto.tardis.apps.ansto_instrument_manager.models import \
    HDF5ParameterMapping
from tardis.tardis_portal.auth.localdb_auth import django_group
from tardis.tardis_portal.models import ObjectACL, ParameterName, Schema, \
    Dataset, DatasetParameter, DatasetParameterSet, ExperimentParameterSet, \
    ExperimentParameter, StorageBox, StorageBoxOption, StorageBoxAttribute, \
    DataFileObject, Experiment, DataFile, DatafileParameterSet, \
    DatafileParameter, Facility, Instrument, ExperimentAuthor
from tardis.tardis_portal.storage.file_system import \
    MyTardisLocalFileSystemStorage

logger = logging.getLogger(__name__)

LOCK_EXPIRE = 60 * 60 * 24  # Lock expires in 24 hours
ANSTO_CLIENT = ANSTOClient.get_initialised_client()
ANSTO_WEBSERVICE = ANSTOWebService()
CACHE = caches['celery-locks']
LOCK_ID = 'ansto-ingest-lock'


def acquire_lock(proposal_id):
    return CACHE.add(LOCK_ID+str(proposal_id), 'true', LOCK_EXPIRE)


def release_lock(proposal_id):
    CACHE.delete(LOCK_ID+proposal_id)


@task(name="ansto.ingest_all_proposals", ignore_result=True)
def ingest_all_proposals():
    """
    Queries the API and ingests all proposals offered for ingestion
    """
    proposals_to_ingest = ANSTO_CLIENT.list_proposals_to_ingest()

    for proposal_id in proposals_to_ingest:
        if acquire_lock(proposal_id):
            try:
                with transaction.atomic():
                    delete_existing_proposal(proposal_id)
                    exp = create_experiment_for_proposal(proposal_id)
                    add_authors_from_proposaldb(exp, proposal_id)
                    add_proposal_group_acl(exp, proposal_id)
                    ingest_files_for_proposal(exp, proposal_id)
                    ANSTO_CLIENT.flag_proposal_as_ingested(proposal_id)
            finally:
                release_lock(proposal_id)


def add_authors_from_proposaldb(exp, proposal_id):
    # Add authors as recorded by the proposals database
    author_order = 0
    for name, institution, email in \
                    ANSTO_WEBSERVICE.get_proposal_investigators(
                        proposal_id) or []:
        ExperimentAuthor(
            experiment=exp,
            author=name,
            institution=institution,
            email=email,
            order=author_order
        ).save()

        if author_order == 0:
            exp.institution_name = institution
            exp.save()

        author_order += 1


def get_experiment_proposal_schema_and_parameter_name():
    experiment_schema, _ = Schema.objects.get_or_create(
        namespace=getattr(settings, 'ANSTO_EXPERIMENT_SCHEMA_NAMESPACE',
                          default_settings.ANSTO_EXPERIMENT_SCHEMA_NAMESPACE),
        type=Schema.EXPERIMENT,
        defaults={'name': 'Experiment ID'}
    )
    experiment_proposal_parameter_name, _ = ParameterName.objects.get_or_create(
        schema=experiment_schema,
        name='Proposal',
        full_name='Proposal',
        data_type=ParameterName.STRING,
        defaults={'is_searchable': True}
    )
    return experiment_proposal_parameter_name, experiment_schema


def get_experiment_proposal_id(exp):
    try:
        proposal_parameter_name, _ = \
            get_experiment_proposal_schema_and_parameter_name()
        proposal_parameter = ExperimentParameter.objects.get(
            name=proposal_parameter_name,
            parameterset__experiment=exp
        )
        return proposal_parameter.string_value
    except ExperimentParameter.DoesNotExist:
        return None

def set_experiment_proposal_id(exp, proposal_id):
    experiment_proposal_parameter_name, experiment_schema = \
        get_experiment_proposal_schema_and_parameter_name()

    parameter_set = ExperimentParameterSet(experiment=exp,
                                           schema=experiment_schema)

    parameter_set.save()
    experiment_parameter = ExperimentParameter(
        name=experiment_proposal_parameter_name,
        string_value=proposal_id,
        parameterset=parameter_set)
    experiment_parameter.save()


def set_experiment_booking_data(exp, bookings):
    booking_schema, _ = Schema.objects.get_or_create(
        namespace=getattr(settings, 'ANSTO_BOOKING_SCHEMA_NAMESPACE',
                          default_settings.ANSTO_BOOKING_SCHEMA_NAMESPACE),
        type=Schema.EXPERIMENT,
        defaults={'name': 'Instrument booking'}
    )
    booking_instrument_parameter_name, _ = ParameterName.objects.get_or_create(
        schema=booking_schema,
        name='instrument',
        full_name='Instrument',
        data_type=ParameterName.STRING,
        defaults={'is_searchable': True,
                  'order': 0}
    )
    booking_start_date_parameter_name, _ = ParameterName.objects.get_or_create(
        schema=booking_schema,
        name='start_date',
        full_name='Start date',
        data_type=ParameterName.STRING,
        defaults={'is_searchable': False,
                  'order': 1}
    )
    booking_end_date_parameter_name, _ = ParameterName.objects.get_or_create(
        schema=booking_schema,
        name='end_date',
        full_name='End date',
        data_type=ParameterName.STRING,
        defaults={'is_searchable': False,
                  'order': 2}
    )

    for booking in bookings:
        parameter_set = ExperimentParameterSet(experiment=exp,
                                               schema=booking_schema)

        parameter_set.save()
        instrument_parameter = ExperimentParameter(
            name=booking_instrument_parameter_name,
            string_value=booking['instrument'],
            parameterset=parameter_set)
        instrument_parameter.save()

        booking_start_parameter = ExperimentParameter(
            name=booking_start_date_parameter_name,
            string_value=booking['start'].strftime("%d/%m/%Y"),
            parameterset=parameter_set)
        booking_start_parameter.save()

        booking_end_parameter = ExperimentParameter(
            name=booking_end_date_parameter_name,
            string_value=booking['end'].strftime("%d/%m/%Y"),
            parameterset=parameter_set)
        booking_end_parameter.save()


def create_experiment_for_proposal(proposal_id, ingest_abstract=False):
    admin_user, _ = User.objects.get_or_create(
        username=getattr(settings, 'ANSTO_API_ADMIN_USER',
                         default_settings.ANSTO_API_ADMIN_USER))
    exp_start, exp_end = ANSTO_WEBSERVICE.get_proposal_date_range(proposal_id)

    if ingest_abstract:
        description = unicode(' <br/> '.join(
            [s for s in [ANSTO_WEBSERVICE.get_proposal_text(proposal_id),
                         ANSTO_WEBSERVICE.get_proposal_references(proposal_id)]
             if s]
        ))
    else:
        description = ''

    exp = Experiment(
        title=ANSTO_WEBSERVICE.get_proposal_title(proposal_id) or proposal_id,
        institution_name=settings.DEFAULT_INSTITUTION,
        created_by=admin_user,
        description=description
      )
    if exp_start:
        exp.start_time = exp_start
        if exp_end:
            exp.end_time = exp_end
    exp.save()

    set_experiment_proposal_id(exp, proposal_id)
    set_experiment_booking_data(
        exp, ANSTO_WEBSERVICE.get_proposal_bookings(proposal_id))

    return exp


def delete_existing_proposal(proposal_id):
    experiment_proposal_parameter_name, experiment_schema = \
        get_experiment_proposal_schema_and_parameter_name()

    # Delete experiment if it already exists; it's being reingested
    experiments = [ps.experiment for ps in
                   ExperimentParameterSet.objects.filter(
                       experimentparameter__string_value=proposal_id,
                       experimentparameter__name=experiment_proposal_parameter_name)]
    for exp in experiments:
        ExperimentAuthor.objects.filter(experiment=exp).delete()
        datasets = Dataset.objects.filter(experiments=exp)
        for ds in datasets:
            # Only delete the dataset if it's not linked to any other
            # experiment
            if ds.experiments.count() == 1:
                # This will cascade to datafiles and datafileobjects
                ds.delete()
        exp.delete()


def get_or_create_dataset(experiment, proposal_id, instrument, sample_name):
    title = "%s / %s" % (sample_name, instrument.name)

    def create_new_dataset():
        dataset_schema, _ = Schema.objects.get_or_create(
            namespace=getattr(settings, 'ANSTO_DATASET_SCHEMA_NAMESPACE',
                              default_settings.ANSTO_DATASET_SCHEMA_NAMESPACE),
            name="Experiment ID",
            type=Schema.DATASET
        )
        dataset_proposal_parameter_name, _ = \
            ParameterName.objects.get_or_create(
                schema=dataset_schema,
                name='Proposal',
                full_name='Proposal',
                data_type=ParameterName.STRING,
                defaults={'is_searchable': True}
            )
        destination_dataset = Dataset(
            description=title,
            instrument=instrument
        )
        destination_dataset.save()
        # Add the proposal ID to each dataset
        parameter_set = DatasetParameterSet(
            dataset=destination_dataset,
            schema=dataset_schema)
        parameter_set.save()
        dataset_parameter = DatasetParameter(
            name=dataset_proposal_parameter_name,
            string_value=proposal_id,
            parameterset=parameter_set)
        dataset_parameter.save()
        destination_dataset.experiments.add(experiment)
        destination_dataset.save()  # Save again to update search index
        return destination_dataset

    try:
        return Dataset.objects.get(experiments=experiment,
                                   description=title,
                                   instrument=instrument)
    except Dataset.DoesNotExist:
        return create_new_dataset()


def ingest_files_for_proposal(experiment, proposal_id):
    """
    Ingests the files for a given proposal, assigning them to an experiment
    :param experiment: the experiment that the files belong to
    :param proposal_id: the ANSTO proposal id
    """
    ansto_client = ANSTOClient.get_initialised_client()

    storage_box, created = StorageBox.objects.get_or_create(
        name="ANSTO remote storage",
        django_storage_class='%s.%s' % (
            MyTardisLocalFileSystemStorage.__module__,
            MyTardisLocalFileSystemStorage.__name__),
        max_size=0
    )

    if created:
        try:
            StorageBoxOption(storage_box=storage_box,
                             key="location",
                             value=settings.ANSTO_STORAGE_PATH).save()
        except AttributeError:
            raise ImproperlyConfigured("ANSTO_STORAGE_PATH is not defined")
        StorageBoxAttribute(storage_box=storage_box,
                            key="can_delete",
                            value="false").save()

    files_to_ingest = ansto_client.list_files_for_proposal(proposal_id)
    file_path_regex = getattr(settings, 'ANSTO_FILE_REGEX',
                              default_settings.ANSTO_FILE_REGEX)
    if file_path_regex:
        pattern = re.compile(file_path_regex)
        files_to_ingest = [pattern.search(f).group(1) for f in files_to_ingest]

    temp_dataset = Dataset(description="Uncategorised")
    temp_dataset.save()
    temp_dataset.experiments.add(experiment)

    for f in files_to_ingest:
        df = DataFile(
            dataset=temp_dataset,
            filename=f.split(os.path.sep)[-1],
            size=os.path.getsize(os.path.join(settings.ANSTO_STORAGE_PATH, f)))
        df.save(require_checksums=False)

        dfo = DataFileObject(
            datafile=df,
            storage_box=storage_box,
            uri=f
        )
        dfo.save()
        dfo.verify()

        # Only process files we know are HDF format
        if is_readable(df):
            instrument, sample_name = get_instrument_and_sample_name(df)
            df.dataset = get_or_create_dataset(
                experiment,
                proposal_id,
                instrument,
                sample_name
            )
            df.save()

            extract_hdf5_metadata(
                df,
                HDF5ParameterMapping.get_mappings_for_datafile(df))

    if temp_dataset.datafile_set.count() == 0:
        temp_dataset.delete()


def is_readable(datafile):
    """
    Returns true if the datafile is an HDF file or a tar file containing
    exactly one HDF file
    :param datafile: the datafile
    :return: true if readable HDF file
    """
    if datafile.mimetype == "application/x-hdf":
        return True
    elif datafile.mimetype == "application/x-tar":
        with tarfile.open(mode='r', fileobj=datafile.get_file(
                verified_only=False)) as \
                tar_file:
            m = magic.Magic(mime_and_encoding=True)
            hdf_count = 0
            for f in tar_file.getmembers():
                member = tar_file.extractfile(f)
                mimetype = m.from_buffer(member.read(1024))
                if mimetype.startswith("application/x-hdf"):
                    hdf_count += 1
                    if hdf_count > 1:
                        return False
            return hdf_count == 1


def extract_hdf5_metadata(datafile, parameter_mappings):
    """
    Extracts metadata according to a predefined map from HDF datafiles
    :param datafile: the HDF datafile
    :param parameter_mappings: HDF path to tardis parameter mappings
    """

    def do_extraction(filename):
        hdf5_file = h5py.File(filename, 'r')
        try:
            data_id, data_root = hdf5_file.iteritems().next()
        except StopIteration:
            logger.warn("File %s seems to be empty!" % filename)
            return

        # Iterate over schemas and HDF5/Parameter mappings
        for mapping in parameter_mappings:
            schema = mapping.datafile_parameter_name.schema
            parameterset, _ = DatafileParameterSet.objects.get_or_create(
                schema=schema, datafile=datafile)
            parameter_name = mapping.datafile_parameter_name
            # Load the HDF5 data from the corresponding path
            try:
                data = data_root[mapping.hdf5_path.lstrip('/')]
            except KeyError:
                logger.warn(
                    "HDF5 path %s not found in datafile %s; some HDF5 "
                    "data may be omitted." % (mapping.hdf5_path, datafile))

                # Delete the parameterset if no data has been added yet
                parameter_count = DatafileParameter.objects.filter(
                    parameterset=parameterset).count()
                if parameter_count == 0:
                    parameterset.delete()

                continue

            # Identify the appropriate data operation; an invalid entry
            # results in 'none' being applied and a warning generated.
            processed_data = mapping.apply_operation(data)

            # Create or update the parameter, setting the value to
            # the result of the operation function
            parameter, _ = DatafileParameter.objects.get_or_create(
                parameterset=parameterset, name=parameter_name)
            parameter.set_value(processed_data)
            parameter.save()

    # Since the HDF5 libraries all require filenames instead of file objects,
    # be smart here and get a local copy of the file only if necessary
    try:
        with get_file_path(datafile) as fname:
            do_extraction(fname)
    except IOError as e:
        logger.warn("Could not process file %s due to an IO error: %s" % (
            datafile, e.message))


def add_proposal_group_acl(exp, proposal_id):
    """
    Adds the appropriate group ACL to the experiment
    :param exp: experiment to which the ACL is added
    :param proposal_id: the ANSTO proposal id
    """
    try:
        # create vbl group
        acl = ObjectACL.objects.filter(
            content_type=exp.get_ct(),
            object_id=exp.id,
            pluginId='ansto_group',
            entityId=proposal_id,
            canRead=True,
            aclOwnershipType=ObjectACL.SYSTEM_OWNED)
        if len(acl) == 0:
            acl = ObjectACL(
                content_type=exp.get_ct(),
                object_id=exp.id,
                pluginId='ansto_group',
                entityId=proposal_id,
                canRead=True,
                aclOwnershipType=ObjectACL.SYSTEM_OWNED)
            acl.save()
    except Exception:
        logger.exception('trouble creating proposal ACL')

    try:
        beamline_group = "ANSTO_GROUP"
        group, created = Group.objects.get_or_create(name=beamline_group)
        # beamline group
        ObjectACL.objects.get_or_create(
            content_type=exp.get_ct(),
            object_id=exp.id,
            pluginId=django_group,
            entityId=str(group.id),
            canRead=True,
            aclOwnershipType=ObjectACL.SYSTEM_OWNED)

        # finally, always add acl for admin group
        group, created = Group.objects.get_or_create(name='admin')
        ObjectACL.objects.get_or_create(
            content_type=exp.get_ct(),
            object_id=exp.id,
            pluginId=django_group,
            entityId=str(group.id),
            isOwner=True,
            canRead=True,
            aclOwnershipType=ObjectACL.SYSTEM_OWNED)
    except Exception:
        logger.exception('trouble creating beamline and admin ACLs')


def get_instrument_and_sample_name(datafile):
    """
    Returns an instrument object and sample name for a given datafile
    :param datafile: an HDF datafile
    :return: the instrument object and sample name
    """
    with get_file_path(datafile) as fname:
        hdf5_file = h5py.File(fname, 'r')
        try:
            data_id, data_root = hdf5_file.iteritems().next()
        except StopIteration:
            data_root = {}

        try:
            # Try getting the instrument name directly from the file
            instrument = data_root['instrument/name'][0]
        except KeyError:
            # If that failes, try inferring from the filename
            instrument_name_map = {
                'ECH': 'echidna',
                'PLP': 'platypus',
                'KWR': 'kowari',
                'QKK': 'quokka',
                'WBT': 'wombat',
                'TPN': 'taipan'
            }
            instrument = instrument_name_map.get(datafile.filename[:3],
                                                 'UNKNOWN')
        try:
            sample_name = data_root['sample/name'][0]
        except KeyError:
            sample_name = None
        if not sample_name:
            sample_name = 'UNKNOWN'

        facility, _ = Facility.objects.get_or_create(name="ANSTO", defaults={
            'manager_group': Group.objects.get_or_create(name='admin')[0]
        })
        instrument, _ = Instrument.objects.get_or_create(
            facility=facility,
            name=instrument.lower())

        return instrument, sample_name


@contextmanager
def get_file_path(datafile, directory=None):
    """
    Gets the path to a datafile or file-like object, downloading to temporary
    storage if required
    :param datafile: a datafile
    :param directory: store the file in the given directory if copied as a
    temp file
    """
    storage_instance = datafile.get_default_storage_box().get_initialised_storage_instance()
    if not isinstance(storage_instance, FileSystemStorage):
        with datafile.get_as_temporary_file(directory) as f:
            yield f.name
    else:
        yield datafile.get_absolute_filepath()
