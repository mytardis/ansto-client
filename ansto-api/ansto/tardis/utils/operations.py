"""
This code was taken from vbl_metaman_HDF.py in the old ANSTO code
"""

def first(list):
    if len(list):
        return list[0]
    return 'XXX'


def last(list):
    if len(list):
        return list[-1]
    return 'XXX'


def ave(list):
    if len(list):
        return float(sum(list) / float(len(list)))
    return 'XXX'


def last_plus_1(list):
    if len(list):
        return list[-1] + 1
    return 'XXX'


ALL_VALUES_ZERO = "OH GOD WHY IS THIS HAPPENING"


def ave_non_zero_fail_on_all_zero(list):
    # Check for non-zero numbers in the array, if they are non-zero, then use
    # average of all numbers. If they are zeros, look to "ctrlLp1_value" and
    # "ctrlLp2_value" for non-zero values and use average of whichever is non-zero,
    # if they both have non-zero values, take average of both "ctrlLp1_value" and "ctrlLp2_value" array
    non_zero = [l for l in list if l != 0]
    if len(non_zero):
        return ave(list)
    else:
        return ALL_VALUES_ZERO


def none(list):
    if len(list):
        return list[0]
    return 'XXX'


def length(list):
    return len(list)