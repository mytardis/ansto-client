from django.contrib import admin
from django.contrib.admin import TabularInline
from django.db import models

from ansto.tardis.utils import operations
from tardis.tardis_portal.models import ParameterName, Instrument


class InstrumentProxyModel(Instrument):
    """
    This model is a proxy to the Instrument model, added because models cannot
    be registered with the django admin twice
    """

    class Meta:
        proxy = True
        verbose_name = 'Instrument'
        verbose_name_plural = 'Instruments'


class HDF5ParameterMapping(models.Model):
    OPERATION_CHOICES = [
        ('first', 'First element'),
        ('last', 'Last element'),
        ('ave', 'Average'),
        ('last_plus_1', 'Last plus 1'),
        ('ave_non_zero_fail_on_all_zero', 'Average of all non-zero numbers'),
        ('length', 'Vector length'),
        ('none', 'None')
    ]

    class Meta:
        verbose_name = 'HDF5 parameter mapping'
        verbose_name_plural = 'HDF5 parameter mappings'

    instruments = models.ManyToManyField(InstrumentProxyModel, blank=True)
    operation = models.CharField('Operation', max_length=50,
                                 choices=OPERATION_CHOICES, default='none',
                                 null=False, blank=False)
    datafile_parameter_name = models.ForeignKey(ParameterName, null=False,
                                                blank=False)
    hdf5_path = models.CharField('HDF5 data path', max_length=255, null=False,
                                 blank=False)

    def __unicode__(self):
        return 'Map HDF5 param %s to %s' % (
            self.hdf5_path, self.datafile_parameter_name.name)

    def apply_operation(self, data):
        return getattr(operations, self.operation)(data)

    def _has_change_perm(self, user_obj):
        return user_obj.has_perm('tardis_acls.change_instrument',
                                 self.instrument)

    @classmethod
    def get_mappings_for_datafile(cls, datafile):
        """
        Gets the mappings for a datafile
        :type datafile: Datafile
        :param datafile: datafile
        :return: the mappings for the datafile
        """
        # first try to get mappings based on containing dataset's instrument
        return cls.objects.filter(
            instruments__dataset__datafile=datafile).distinct()


class HDF5ParameterMappingInline(TabularInline):
    model = HDF5ParameterMapping.instruments.through
    extra = 0
    verbose_name = 'HDF5 parameter mapping'
    verbose_name_plural = 'HDF5 parameter mappings'


@admin.register(HDF5ParameterMapping)
class HDF5ParameterMappingAdmin(admin.ModelAdmin):
    pass


@admin.register(InstrumentProxyModel)
class InstrumentAdmin(admin.ModelAdmin):
    inlines = [HDF5ParameterMappingInline]
