from django.core.exceptions import ValidationError
from rest_framework import serializers

from ansto.tardis.apps.ansto_instrument_manager.models import \
    HDF5ParameterMapping
from tardis.tardis_portal.models import Schema, Instrument, ParameterName, \
    Facility


class HDF5ParameterMappingSerializer(serializers.ModelSerializer):
    class Meta:
        model = HDF5ParameterMapping

    def validate_datafile_parameter_name(self, param_name):
        if param_name.schema.type != Schema.DATAFILE:
            raise ValidationError(
                    'Parameter name provided is not for a datafile')
        return param_name

    def get_fields(self):
        fields = super(HDF5ParameterMappingSerializer, self).get_fields()

        # Restrict the available choices of instrument and parameter name
        # These are also enforced by the HDF5ParameterMappingPermissions
        # permissions class
        user = self.context['view'].request.user
        fields['instruments'].queryset = Instrument.objects.filter(
                facility__manager_group__in=user.groups.all())
        fields[
            'datafile_parameter_name'].queryset = ParameterName.objects.filter(
                schema__type=Schema.DATAFILE)

        return fields


class InstrumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Instrument

    def get_fields(self):
        fields = super(InstrumentSerializer, self).get_fields()

        # Restrict the available choices of facility
        # These are also enforced by the HDF5ParameterMappingPermissions
        # permissions class
        user = self.context['view'].request.user
        fields['facility'].queryset = Facility.objects.filter(
                manager_group__in=user.groups.all())

        return fields


class SchemaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schema


class ParameterNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = ParameterName


class FacilitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Facility
