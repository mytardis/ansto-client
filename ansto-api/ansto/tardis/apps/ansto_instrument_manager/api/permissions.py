from rest_framework import permissions

from tardis.tardis_portal.models import Instrument, Facility


class HDF5ParameterMappingPermissions(permissions.BasePermission):
    def has_permission(self, request, view):
        if not request.user.is_authenticated():
            return False

        if request.method not in permissions.SAFE_METHODS:
            try:
                instrument_pk = request.data.get('instrument')
                if instrument_pk:
                    instrument = Instrument.objects.get(pk=instrument_pk)
                    return request.user.has_perm(
                            'tardis_acls.change_instrument', instrument)
            except Instrument.DoesNotExist:
                return False
        return True

    def has_object_permission(self, request, view, obj):
        return request.user.has_perm('tardis_acls.change_hdf5parametermapping',
                                     obj)


class InstrumentPermissions(permissions.BasePermission):
    def has_permission(self, request, view):
        if not request.user.is_authenticated():
            return False

        if request.method not in permissions.SAFE_METHODS:
            try:
                facility_pk = request.data.get('facility')
                if facility_pk:
                    Facility.objects.get(pk=facility_pk,
                                         manager_group__in=request.user.groups.all())
            except Facility.DoesNotExist:
                return False
        return True

    def has_object_permission(self, request, view, obj):
        return request.user.has_perm('tardis_acls.change_instrument', obj)


class SchemaPermissions(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.has_perm('tardis_acls.change_schema')


class IsAdminOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS and request.user.is_authenticated():
            return True
        elif request.user.is_superuser:
            return True
        return False
