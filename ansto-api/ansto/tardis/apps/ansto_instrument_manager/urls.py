from django.conf.urls import url, include
from rest_framework import routers

from ansto.tardis.apps.ansto_instrument_manager import views

router = routers.DefaultRouter()
router.register(r'mappings', views.HDF5ParameterMappingViewSet,
                'HDF5 Parameter Mappings')
router.register(r'instruments', views.InstrumentViewSet, 'Instruments')
router.register(r'schemas', views.SchemasViewSet, 'Schemas')
router.register(r'parameternames', views.ParameterNameViewSet,
                'Parameter Names')
router.register(r'facilities', views.FacilityViewSet, "Facilities")

urlpatterns = [
    url(r'^$', views.IndexView.as_view()),
    url(r'^', include(router.urls)),
    url(r'^api-auth/$',
        include('rest_framework.urls', namespace='rest_framework'))
]
