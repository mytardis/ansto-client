# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tardis_portal', '0009_auto_20160128_1119'),
    ]

    operations = [
        migrations.CreateModel(
            name='HDF5ParameterMapping',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('operation', models.CharField(default=b'none', max_length=50, verbose_name=b'Operation', choices=[(b'first', b'First element'), (b'last', b'Last element'), (b'ave', b'Average'), (b'last_plus_1', b'Last plus 1'), (b'ave_non_zero_fail_on_all_zero', b'Average of all non-zero numbers'), (b'none', b'None')])),
                ('hdf5_path', models.CharField(max_length=255, verbose_name=b'HDF5 data path')),
                ('datafile_parameter_name', models.ForeignKey(to='tardis_portal.ParameterName')),
            ],
            options={
                'verbose_name': 'HDF5 parameter mapping',
                'verbose_name_plural': 'HDF5 parameter mappings',
            },
        ),
        migrations.CreateModel(
            name='InstrumentProxyModel',
            fields=[
            ],
            options={
                'verbose_name': 'Instrument',
                'proxy': True,
                'verbose_name_plural': 'Instruments',
            },
            bases=('tardis_portal.instrument',),
        ),
        migrations.AddField(
            model_name='hdf5parametermapping',
            name='instruments',
            field=models.ManyToManyField(to='ansto_instrument_manager.InstrumentProxyModel', blank=True),
        ),
    ]
