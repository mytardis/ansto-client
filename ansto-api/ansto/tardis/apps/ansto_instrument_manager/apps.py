from tardis.app_config import AbstractTardisAppConfig


class AnstoAppConfig(AbstractTardisAppConfig):
    name = 'ansto.tardis.apps.ansto_instrument_manager'
    verbose_name = 'ANSTO Instrument Manager'
    app_dependencies = ['rest_framework']

    def ready(self):
        # Register signals
        import ansto.tardis.apps.ansto_instrument_manager.signals.handlers #noqa