app.config(['$resourceProvider', function ($resourceProvider) {
    $resourceProvider.defaults.stripTrailingSlashes = false;
}]);

app.factory('BaseResource', ['$q', '$resource', function ($q, $resource) {

    // Returns a promise resolving to a dictionary of key/values for choice fields
    var getFieldChoices = function () {
        console.log(this);
        var deferred = $q.defer();
        if (typeof this.choices === "undefined") {
            var choices = this.choices = {};
            this.options().$promise.then(function (optionsResult) {
                var obj = optionsResult.actions.POST;
                for (var key in obj) {
                    if (obj.hasOwnProperty(key)) {
                        var field = obj[key];
                        if (field.hasOwnProperty('choices')) {
                            choices[key] = {};
                            for (var i = 0; i < field.choices.length; i++) {
                                choices[key][field.choices[i]['value']] = field.choices[i]['display_name'];
                            }
                        }
                    }
                }
                deferred.resolve(choices);
            });
        } else {
            deferred.resolve(this.choices);
        }

        return deferred.promise;
    };

    var Resource = function (resourceUrl) {
        var resource = $resource(resourceUrl, {}, {
            options: {
                method: 'OPTIONS'
            },
            post: {
                method: 'POST'
            }
        });
        this.get = resource.get;
        this.query = resource.query;
        this.options = resource.options;
        this.post = resource.post;
    };
    Resource.prototype.getFieldChoices = getFieldChoices;

    return function (resourceUrl) {
        return new Resource(resourceUrl);
    };

}]);

app.factory('Facility', ['BaseResource', function (BaseResource) {
    return new BaseResource('/apps/ansto-instrument-manager/facilities/:id/');
}]);

app.factory('Instrument', ['BaseResource', '$q', function (BaseResource, $q) {
    var Instrument = BaseResource('/apps/ansto-instrument-manager/instruments/:id/');

    var addFacilitiesToResult = function (result) {
        return Instrument.getFieldChoices().then(function (choices) {
            result.facility_name = choices.facility[result.facility];
            return result.facility;
        })
    };

    return {
        getAccessibleFacilities: function () {
            var deferred = $q.defer();
            console.log(Instrument);
            Instrument.getFieldChoices().then(function (choices) {
                deferred.resolve(choices.facility);
            });
            return deferred.promise;
        },
        getAllInstruments: function (facilityId) {
            var params = {};
            if (typeof facilityId !== "undefined") {
                params = {'facility': facilityId};
            }

            var promise;
            return Instrument.query(params).$promise.then(function (instrumentList) {
                if (instrumentList.length === 0) {
                    return [];
                }
                for (var i = 0; i < instrumentList.length; i++) {

                    var fn = (function (instrument) {
                        return function () {
                            return addFacilitiesToResult(instrument)
                        };
                    })(instrumentList[i]);

                    if (typeof promise === "undefined") {
                        promise = fn();
                    } else {
                        promise = promise.then(fn);
                    }
                }

                return promise.then(function () {
                    return instrumentList;
                });
            });
        },
        getInstrument: function (id) {
            return Instrument.get({'id': id}).$promise.then(function (instrument) {
                return addFacilitiesToResult(instrument)
            })

        },
        createInstrument: function (name, facility) {
            return Instrument.post({
                'name': name,
                'facility': facility
            }).$promise;
        }
    };
}]);

app.factory('Mapping', ['BaseResource', function (BaseResource) {
    var Mappings = BaseResource('/apps/ansto-instrument-manager/mappings/:id/');

    return {
        'getFormChoices': function () {
            return Mappings.getFieldChoices();
        },
        'getAllMappings': function (instrumentId) {
            var params = {};
            if (typeof instrumentId !== "undefined") {
                params = {'instrument': instrumentId};
            }

            return Mappings.query(params).$promise;
        }
    }
}]);

app.controller('InstrumentList', ['$scope', 'Mapping', function ($scope, Mapping) {
    Mapping.getFormChoices().then(function (choices) {
        console.log(choices);
    });
}]);