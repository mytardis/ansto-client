from django.db.models.signals import post_save
from django.dispatch import receiver

from ansto.api.ansto_data_api import ANSTOClient
from ansto.tardis.apps.ansto_instrument_manager.models import \
    HDF5ParameterMapping
from ansto.tardis.utils.ingestion import get_experiment_proposal_id
from tardis.tardis_portal.models import Experiment


ANSTO_CLIENT = ANSTOClient.get_initialised_client()


@receiver(post_save, sender=HDF5ParameterMapping)
def trigger_reingestion(sender, **kwargs):
    hdf5_parameter_mapping = kwargs.get('instance')
    affected_experiments = Experiment.objects.filter(
        datasets__instrument=hdf5_parameter_mapping.instruments.all()
    ).distinct()

    for exp in affected_experiments:
        proposal_id = get_experiment_proposal_id(exp)
        if proposal_id:
            ANSTO_CLIENT.flag_proposal_for_ingestion(proposal_id)
