from django.views.generic import TemplateView
from rest_framework import viewsets, exceptions
from rest_framework.decorators import detail_route
from rest_framework.response import Response


from ansto.tardis.apps.ansto_instrument_manager.api.permissions import \
    HDF5ParameterMappingPermissions, InstrumentPermissions, SchemaPermissions, \
    IsAdminOrReadOnly
from ansto.tardis.apps.ansto_instrument_manager.api.serializers import \
    HDF5ParameterMappingSerializer, InstrumentSerializer, \
    SchemaSerializer, ParameterNameSerializer, FacilitySerializer
from ansto.tardis.apps.ansto_instrument_manager.models import \
    HDF5ParameterMapping
from tardis.tardis_portal.models import Instrument, Schema, ParameterName, \
    Facility


class IndexView(TemplateView):
    template_name = "index.html"


class HDF5ParameterMappingViewSet(viewsets.ModelViewSet):
    serializer_class = HDF5ParameterMappingSerializer
    permission_classes = (HDF5ParameterMappingPermissions,)

    def get_queryset(self):
        queryset = HDF5ParameterMapping.objects.filter(
                instruments__facility__manager_group__in=self.request.user.groups.all())

        instrument_filter = self.request.query_params.get('instrument', None)
        if instrument_filter:
            queryset = queryset.filter(instrument__pk=instrument_filter)

        return queryset


class InstrumentViewSet(viewsets.ModelViewSet):
    serializer_class = InstrumentSerializer
    permission_classes = (InstrumentPermissions,)

    def get_queryset(self):
        queryset = Instrument.objects.filter(
                facility__manager_group__in=self.request.user.groups.all())

        facility_filter = self.request.query_params.get('facility', None)
        if facility_filter:
            queryset = queryset.filter(facility__pk=facility_filter)

        return queryset


class SchemasViewSet(viewsets.ModelViewSet):
    serializer_class = SchemaSerializer
    permission_classes = (SchemaPermissions,)
    queryset = Schema.objects.all()

    @detail_route()
    def parameters(self, request, pk=None):
        parameter_names = ParameterName.objects.filter(
                schema=self.get_queryset().get(pk=pk))
        serializer = ParameterNameSerializer(parameter_names, many=True)
        return Response(serializer.data)


class ParameterNameViewSet(viewsets.ModelViewSet):
    serializer_class = ParameterNameSerializer
    permission_classes = (SchemaPermissions,)

    schema_type_mapping = dict(
            (v.lower(), k) for k, v in dict(Schema._SCHEMA_TYPES_SHORT).iteritems())

    def get_queryset(self):
        queryset = ParameterName.objects.all()
        schema_filter = self.request.query_params.get('schema', None)
        if schema_filter:
            queryset = queryset.filter(schema__pk=schema_filter)

        type = self.request.query_params.get('type', None)
        if type:
            try:
                type_filter = ParameterNameViewSet.schema_type_mapping.get(type,None) or int(type)
                queryset = queryset.filter(schema__type=type_filter)
            except ValueError:
                raise exceptions.ParseError

        return queryset


class FacilityViewSet(viewsets.ModelViewSet):
    serializer_class = FacilitySerializer
    permission_classes = (IsAdminOrReadOnly,)

    def get_queryset(self):
        if self.request.user.is_superuser:
            return Facility.objects.all()
        else:
            return Facility.objects.filter(manager_group__in=self.request.user.groups.all())